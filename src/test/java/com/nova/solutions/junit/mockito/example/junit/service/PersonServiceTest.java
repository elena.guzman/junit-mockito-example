package com.nova.solutions.junit.mockito.example.junit.service;

import com.nova.solutions.junit.mockito.example.model.Person;
import com.nova.solutions.junit.mockito.example.service.PersonService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

@DisplayName("Person Service Test class.")
public class PersonServiceTest {
  
  @DisplayName("Person Test")
  @Test
  void personTest() {
    int id = 1;
    String name = "Daniel";
    String lastName = "Avila";
    String birthDate = "11/11/1995";
    int statusCode = 1;
   
    PersonService personService = new PersonService();
    Person person = personService.person(id, name, lastName, birthDate, statusCode);
    
    Assertions.assertAll("person", ()-> Assertions.assertNotNull(person),
        () -> Assertions.assertEquals(26, person.getAge()),
        () -> Assertions.assertEquals("Request", person.getStatus()));
  }
  
  @DisplayName("Person Test all status")
  @ParameterizedTest
  @CsvSource({
    "1, Daniel, Avila, 11/11/1995, 1, 26, Request",
    "2, Daniel, Avila, 11/11/1994, 2, 27, Pending", 
    "3, Daniel, Avila, 11/11/1996, 3, 25, In Process", 
    "4, Daniel, Avila, 11/11/1992, 4, 29, Complete", 
    "5, Daniel, Avila, 11/11/1990, 5, 31, N/A", 
    "6, Daniel, Avila, 11/11/1995, 6, 26, N/A"})
  void personTestAllStatus(int id, String name, String lastName, String birthDate, int statusCode, int age, String status) {
    PersonService personService = new PersonService();
    Person person = personService.person(id, name, lastName, birthDate, statusCode);
    
    Assertions.assertAll("person", ()-> Assertions.assertNotNull(person),
        () -> Assertions.assertEquals(age, person.getAge()),
        () -> Assertions.assertEquals(status, person.getStatus()));
  }
  
}
