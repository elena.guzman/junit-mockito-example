package com.nova.solutions.junit.mockito.example.util;

import com.nova.solutions.junit.mockito.example.model.NumericCollection;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.List;
import javax.management.RuntimeErrorException;

public class Util {
  
  public static long factorialNumber(long num) {
    long resp = 1l;
    while (num > 0l) {
      resp = resp * num;
      num--;
    }
    return resp;
  }
  
  public static BigDecimal add(String num1, String num2) throws Exception {
    BigDecimal result = new BigDecimal(0l);
    try {
      BigDecimal number1 = new BigDecimal(num1);
      BigDecimal number2 = new BigDecimal(num2);
      result = result.add(number1);
      result = result.add(number2);
    } catch (NumberFormatException ex) {
      throw new NumberFormatException("Error al parsear " + ex.getMessage());
    }
    return result;
  }
  
  public static int getAge(String birthDate) throws Exception {
    int age = 0;

    DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    LocalDate birth = LocalDate.parse(birthDate, format);
    Period period = Period.between(birth, LocalDate.now());
    age = period.getYears();
    
    if(age <= 0) {
      throw new RuntimeErrorException(null, "Error al calcular la edad.");
    }
    
    return age;
  }
  
  public static String getStatus(int statusCode) {
    String status = null;
    switch (statusCode) {
      case 1:
        status = "Request";
        break;
      case 2:
        status = "Pending";
        break;
      case 3:
        status = "In Process";
        break;
      case 4:
        status = "Complete";
        break;
      default:
        status = "N/A";
        break;
    }
    return status;
  }
  
  public static NumericCollection getMapping(List<Integer> numbers) {
    NumericCollection collection = new NumericCollection();

    numbers.stream().distinct().forEach(x -> {
      collection.addToMap(x, 0);
      collection.addToList(x);
    });
    numbers.stream().forEach(x -> {
      Integer value = collection.getValueByKey(x);
      collection.addToMap(x, value + 1);
    });

    return collection;
  }
}
