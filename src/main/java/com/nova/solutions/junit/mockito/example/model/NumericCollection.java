package com.nova.solutions.junit.mockito.example.model;

import lombok.Getter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
public class NumericCollection {
  
  List<Integer> number = new ArrayList<>();
  
  Map<Integer, Integer> mapping = new HashMap<>();
  
  
  public void addToList(Integer num) {
    number.add(num);
  }
  
  public void addToMap(Integer num, Integer value) {
    mapping.put(num, value);
  }
  
  public Integer getValueByKey(Integer key) {
    return mapping.get(key);
  }

}
